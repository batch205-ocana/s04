
class Student{
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.grades = grades;
        this.grades.forEach(item => {
            if(typeof item !== "number"){
                this.grades = undefined;
            }
        });
        this.average = undefined;
        this.isPassed = undefined;
        this.isPassedWithHonors = undefined;
    }
    login(){
        console.log(`${this.email} has logged in`);
        return this;

    }
    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }
    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    }
    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => {
            return accumulator += num;
        })
        this.average = sum/4;        
        return this;
    }
    willPass(){
        this.isPassed =  Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }
    willPassWithHonors(){
        this.isPassedWithHonors = (this.willPass().isPassed && Math.round(this.computeAve().average) >= 90) ? true : this.willPass().isPassed ? false : undefined;
        return this;
    }

}

class Section{
    constructor(name){
        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
        this.passedStudents = undefined;
        this.sectionAve = undefined;
    }
    addStudent(name,email,grades){
        this.students.push(new Student(name,email,grades));
        return this;
    }
    countHonorStudents(){
        let count = 0;
        this.students.forEach(student => {
            student.willPassWithHonors();
            if(student.isPassedWithHonors){
                count++;
            }
        })
        this.honorStudents = count;
        return this;

    }
    getNumberOfStudents(){
        return this.students.length;
    }
    countPassedStudents(){
        let count = 0;
        this.students.forEach(student => {
            student.willPass();
            if(student.isPassed){
                count++;
            }
        })
        this.passedStudents = count;
        return this;
    }
    computeSectionAve(){
        let average = 0;
        this.students.forEach(student => {
            average += student.computeAve().average;
        })
        this.sectionAve = average/this.getNumberOfStudents();
        return this;
    }
}

let section1A = new Section("Section 1A");
section1A.addStudent('Joy','joy@mail.com',[89,91,92,88]);
section1A.addStudent('Jeff','jeff@mail.com',[81,80,82,78]);
section1A.addStudent('John','john@mail.com',[91,90,92,96]);
section1A.addStudent('Jack','jack@mail.com',[95,92,92,93]);
section1A.addStudent('Alex','alex@mail.com',[84,85,85,86]);
// console.log("John's average is: ",section1A.students[2].computeAve().average);
// console.log("Jeff passed: ",section1A.students[3].computeAve().willPass().isPassed);
console.log(section1A);
console.log(section1A.countHonorStudents().honorStudents);
console.log(section1A.getNumberOfStudents());
console.log(section1A.countPassedStudents().passedStudents);
console.log(section1A.computeSectionAve().sectionAve);